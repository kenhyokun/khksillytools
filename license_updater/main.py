import sys
import os

args_license = sys.argv[1]
args_update_dir = sys.argv[2]

print(args_license)
print(args_update_dir)

license_file = open(args_license, 'r')
license_file_lines = license_file.readlines()
license_file_lines.insert(0, "/*\n")
license_file_lines.append("*/\n")

dir_list = os.listdir(args_update_dir)

def update_license(file):
    print(file)
    f = open(file, 'r')
    lines = f.readlines()

    target_index = 0
    for line in lines:
        target_index += 1
        if '*/' in line:
            break

    for i in range(target_index):
        lines.pop(0)

    new_lines = license_file_lines + lines

    with open(file, 'w') as f:
        for line in new_lines:
          f.write(f"{line}")


for file_name in dir_list:
    if '.c' in file_name or '.h' in file_name:
        update_license(args_update_dir + '/' + file_name)

