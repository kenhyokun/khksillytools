## About

Just silly tools that I wrote for run google closure-compiler command recursively.

## Google closure-compiler

 - [google closure-compiler site](https://developers.google.com/closure/compiler/docs/gettingstarted_app)
 - [google closure-compiler maven repository](https://mvnrepository.com/artifact/com.google.javascript/closure-compiler)
  
## Authors

- Kevin Haryo Kuncoro
 

  