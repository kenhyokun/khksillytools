/*
License under zlib license
Copyright (C) 2020-2021 Kevin Haryo Kuncoro

This software is provided 'as-is', without any express or implied
warranty.  In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not
claim that you wrote the original software. If you use this software
in a product, an acknowledgment in the product documentation would be
appreciated but is not required.

2. Altered source versions must be plainly marked as such, and must not be
misrepresented as being the original software.

3. This notice may not be removed or altered from any source distribution.

Kevin Haryo Kuncoro
kevinhyokun91@gmail.com
*/

#include<iostream>
#include<string>
#include<filesystem>

using std::cout;
using std::endl;
using std::filesystem::directory_iterator;
using std::filesystem::current_path;
using std::string;

string src_path = "";
string dst_path = "";

void run_closure_compiler_command(string file_name){
    cout << file_name + " on progress" << endl;

    string output = "";

    if(dst_path != ""){
      output = dst_path + file_name;
    }
    else{
      output = src_path + file_name;
    }

    string closure_compiler_command = "java -jar closure-compiler.jar --js " +
      src_path + file_name + " --js_output_file " +
      output;

    system(closure_compiler_command.c_str());

    cout << file_name + " done" << endl;
}

bool is_js(string file_name){
  std::size_t found = file_name.find(".js");

  if (found != string::npos){
    string file_ext = file_name.substr(found, file_name.length());

    if(file_ext == ".js") // to avoid .json 
      return true;

  }

  return false;
}

void get_file_and_run_closure_compiler(string full_path){
  int last_forward_slash_index = 0;

  for(int i = full_path.length(); i >= 0; --i){
    if(full_path[i] == '/'){
      last_forward_slash_index = i;
      break;
    }
  }

  full_path.erase(0, last_forward_slash_index + 1);

  std::size_t found = full_path.find('.');
  if (found != string::npos){

    if(is_js(full_path)){
      cout << "found file -> " + full_path << endl;
      run_closure_compiler_command(full_path);
    }

  }
  else{
    // cout << "found dir -> " + full_path << endl;
  }

}

int main(int argc, char *argv[]){

  if(argv[1])
    src_path = argv[1];

  if(argv[2])
    dst_path = argv[2];


  if(src_path != "" && dst_path != ""){

    cout << "src path:" << src_path << endl;
    cout << "dst path:" << dst_path << endl;

    for(const auto &file : directory_iterator(src_path)){
      string full_path = file.path();
      get_file_and_run_closure_compiler(full_path);
    }

  }
  else{
    cout << "args:" << endl;
    cout << "[src path] [dst path]" <<endl;
  }

  return 0;
}
