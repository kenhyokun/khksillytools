/*
License under zlib license
Copyright (C) 2020-2021 Kevin Haryo Kuncoro

This software is provided 'as-is', without any express or implied
warranty.  In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not
claim that you wrote the original software. If you use this software
in a product, an acknowledgment in the product documentation would be
appreciated but is not required.

2. Altered source versions must be plainly marked as such, and must not be
misrepresented as being the original software.

3. This notice may not be removed or altered from any source distribution.

Kevin Haryo Kuncoro
kevinhyokun91@gmail.com
*/

#include<iostream>
#include<ios>
#include<fstream>
#include<string>
#include<vector>

using std::cout;
using std::endl;
using std::string;
using std::ifstream;
using std::ofstream;
using std::vector;

vector<string> order_list;

void add_order_list(string line, void *nullptr_param){
  order_list.push_back(line);
}

void write_line(string line, void *param){
  string dst_file = (const char*)param;
  ofstream file(dst_file, std::ios_base::app | std::ios_base::out);

  if(file.is_open()){
    file << line;
    file << "\n";
  }
}

void read_file(string src_file,
	       void (*callback)(string, void*) = nullptr,
	       void *callback_param = nullptr){
  string line;
  ifstream file(src_file);

  if(file.is_open()){
    while(getline(file, line)){
      if(callback) (*callback)(line, callback_param);
    }
    file.close();
  }
  else{
    cout << src_file << ":file not found!" << endl;
  }
}

//NOTE(Kevin):
// argv[1] = source directory 
// argv[2] = output file on source directory
int main(int argc, char* argv[]){

  if(argv[1] && argv[2]){

    string src_dir = argv[1];
    const char *dst_file = argv[2];

    read_file(src_dir + "order_list", add_order_list);

    for(int i = 0; i < order_list.size(); ++i){
      string src_file = order_list[i];
      read_file(src_dir + src_file, write_line, (void*)dst_file);
    }

  }
  else{
    cout << "args:" << endl;
    cout << "[src path] [dst file name]" <<endl;
  }

  return 0;
}
