/*
License under zlib license
Copyright (C) 2022 Kevin Haryo Kuncoro

This software is provided 'as-is', without any express or implied
warranty.  In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not
claim that you wrote the original software. If you use this software
in a product, an acknowledgment in the product documentation would be
appreciated but is not required.

2. Altered source versions must be plainly marked as such, and must not be
misrepresented as being the original software.

3. This notice may not be removed or altered from any source distribution.

Kevin Haryo Kuncoro
kevinhyokun91@gmail.com
*/

#include<dirent.h>
#include<iostream>
#include<algorithm>
#include<cstring>
#include<filesystem>

using std::cout;
using std::endl;
using std::string;
using std::filesystem::directory_iterator;
using std::filesystem::current_path;

string get_dir_name(string src_path){
  string dir_name = src_path.substr(src_path.find_last_of("/\\") + 1);
  return dir_name;
}

int main(int argc, char *argv[]){

  string src_path = "";

  if(argv[1])
    src_path = argv[1];

  if(src_path != ""){
    for(const auto &file : directory_iterator(src_path)){
      string full_path = file.path();
      string index_html_file_path = full_path + "/index.html";
      string grep_line_command = "grep -n 'manifest' " + index_html_file_path + " | cut -d : -f 1";

      DIR *dir = nullptr;
      dir = opendir(full_path.c_str());

      char buffer[128]; 
      string result = "";

      if(dir){

	dirent *entry = nullptr;
	entry = readdir(dir);

	if(strcmp(entry->d_name, ".") != 0 &&
            get_dir_name(full_path) != ".git"){ // avoid hidden directory and .git directory

	  FILE *pipe = popen(grep_line_command.c_str(), "r");
	  if(!pipe) throw std::runtime_error("popen() failed!");

	  try {
	    while (fgets(buffer, sizeof buffer, pipe) != NULL) {
	      result += buffer;
	    }
	  } catch (...) {
	    pclose(pipe);
	    throw;
	  }

	  pclose(pipe);
	}

      }

      free(dir);

      if(result != ""){ 

        result.erase(remove_if(result.begin(), result.end(), isspace), result.end());

        char quot = 39;
        string sed_command = "sed ";
        sed_command.append(1, quot);
        sed_command.append(result);
        sed_command.append(1, 'd');
        sed_command.append(1, quot);
        sed_command.append(" ");
        sed_command.append(full_path);
        sed_command.append("/index.html");
        sed_command.append(" >> ");
        sed_command.append(full_path);
        sed_command.append("/_index.html");
        system(sed_command.c_str());

        string rm_command = "rm ";
        rm_command.append(full_path);
        rm_command.append("/index.html");
        system(rm_command.c_str());

        string mv_command = "mv ";
        mv_command.append(full_path);
        mv_command.append("/_index.html ");
        mv_command.append(full_path);
        mv_command.append("/index.html");
        system(mv_command.c_str());

        cout << index_html_file_path << endl;
      }

    }
  }
  else{
    cout << "args:" << endl;
    cout << "[src path]" << endl;
  }

  return 0;
}


